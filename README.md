### Node-Pi-System-Stats ###

This project uses NodeJs to collect system statistics and store them in a database, it then provides an express server to display these system statistics. The core idea of this application is to be self contained using as much javascript as possible and have no dependencies outside of requireing nodejs to be installed.

### How do I get set up? ###

1) Install nodeJs on your system.  
1.1) curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -   
1.2) sudo apt install nodejs  
2) Run "sudo npm install"  
3) node index.js  

or run as a background task
4) node server.js > stdout.txt 2> stderr.txt &

### Configuration ###
The port can be changed.  
The database filename can be changed.  

### Issues ###
On installation the sqlite3 package could not be installed, running "sudo npm install sqlite3 --save" caused sqlite3 to be built correctly. (This was only required os RaspianOS)

### Run as a service ###

useradd -s /bin/bash -m -d /home/safeuser -c "safe user" safeuser
passwd safeuser
usermod -aG sudo <username>

sudo npm install pm2 -g

log in as safeuser and run 

pm2 start app.js

//to start pm2 on reboot you will need

pm2 startup

