//DEBUG=* node index.js

// FUTURE FEATURES START
//TODO: Possible features, if we can get the client IP address, include that in the list and that could provide a link if localserver is enabled.
//Continued, this could also listen for requests to post the last 30 days worth of data to the "hub" if viewing is required.
//TODO: Possibly look at adding some sort of security as the hub can recieve messages from anyone.
// FUTURE FEATURES END

const Config = {
  enableLocalServer: true,
  localPort: '3000',
  database: true,
  hubStatus: true,         // true means this will act as a Hub, else false
  hubIp: '192.168.0.9',    // if set this requires a string.       hubIp :'192.168.0.1'
  hubPort: '3000',         // if set requires a port as a string.  hubPort : '3000'
};

const os = require('os');  //Used for hostname see below.
const osUtils  = require('os-utils'); // Used for fast prototype - can be removed once cpuUsage AND cpuFree are converted to the above.
var CronJob = require('node-cron');

var express = require('express');
var app = express();
var path = __dirname + '/public_html/';

var http = require('http').Server(app);
var io = require('socket.io')(http);

var ioc = require('socket.io-client');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('Database.db');

//Create database
db.serialize(function() {
  db.run("CREATE TABLE if not exists system_stats (id integer primary key autoincrement, free_mem REAL, total_mem REAL, cpu_usage REAL, cpu_free REAL, time_now INTEGER)");
});

//If it is a hub then receive and bounce messages, else configure as a client to emit to hub.
if(Config.hubStatus) {
  io.on('connection', function (socket) {
    
    socket.on('client-data', function (data) {
      io.emit('peer-data', data);
    });
  });
}else if(Config.hubIp != null && Config.hubPort != null){
  var ioClient = ioc.connect('http://'+ Config.hubIp +':'+ Config.hubPort);
}

//Grab the no async data and return as a object of mutiple items
function staticSystemData() {
  var data = {};

  data.time = Date.now();
  data.hostname = os.hostname();
  data.platform = os.platform();
  data.cpuCount = os.cpus().length;
  data.upTime = Math.floor(os.uptime()); 
  data.memoryFree = os.freemem();
  data.memoryTotal = os.totalmem();
  data.memoryUsage = Math.round(((os.totalmem() - os.freemem()) / os.totalmem()) * 100);

  return data;
}

//TODO: This could be done and reduce uusing the OS-utils module - requires to check if all available data is in the os cpu object.
//Grab the current Cpu usage
function cpuUsage() {
  return new Promise((resolve, reject) => {
    osUtils.cpuUsage(function(x){
      var result = Math.round(x * 100);
      resolve(result);
    });
  });
}

//TODO: This could be done and reduce uusing the OS-utils module - requires to check if all available data is in the os cpu object.
//Grab the current free Cpu ssage
function cpuFree() {
  return new Promise((resolve, reject) => { 
    osUtils.cpuFree(function(x){
        var result = Math.round(x * 100);
        resolve(result);
    });
  });
}

//Send our object and save the required data.
function saveDataDb(data) {
  db.run("INSERT INTO system_stats (free_mem, total_mem, cpu_usage, cpu_free, time_now) VALUES (?, ?, ?, ?, ?)", data.memoryFree, data.memoryTotal, data.cpuUsage, data.cpuFree, Date.now(), function (err) {
    if (err) {
        console.log(err.message);
        return;
    }
  });
}

//calculate and deduct the number of months to prune the data by.
function deductTime() {
  var d = new Date();
  d.setMonth(d.getMonth() - 1);
  d.getTime();
  return d;
}

//Database statement to delete records.
function trimDatabase() {
  db.run("DELETE FROM system_stats WHERE time_now >= ?", deductTime()+")", function (err) {
    if (err) {
        console.log(err.message);
        return;
    }
  });
}

//TODO: We want a gracefull message and to try/catch any isssues that may arise and post this to the front end and check for an error emit. -This needs concideration as the client to hub may be an issue...maybe discard the emit if bad data. 
//We want to pull all the data and combine it as one object to use in database storage or to emit one single payload.
async function combineData() {
  var data = await staticSystemData();
  data.cpuUsage = await cpuUsage();
  data.cpuFree = await cpuFree();
  return data;
}

//Run a Cronjob every 5 secs to grab our statistics.
var job = CronJob.schedule('*/5 * * * * *', function(){
  //Once we have all the data decide what to do
  combineData().then(data => {
    //transmit the data to the front end and to the Hub
    io.emit('data', data);
    if(!Config.hubStatus && Config.hubIp != null && Config.hubPort != null) {
      ioClient.emit('client-data', data);
    }
  });
});

//Run a Cronjob every 5 minutes to grab our statistics.
var jobSaveData = CronJob.schedule('*/5 * * * *', function(){
  //Once we have all the data decide what to do
  combineData().then(data => {
    //If database access to enabled then save to Db
    if(Config.database){
      saveDataDb(data);
      trimDatabase();
    }
  });
});

job.start();
jobSaveData.start();

////////////////////////////////////////////
//////////// Express used below ////////////
////////////////////////////////////////////

//If it is a node we can not bother using the local server.
if(Config.enableLocalServer){
  //Server - Assests to serve, like js/css etc.
  app.use(express.static('public_html'));

  //Server - pages to serve
  app.get('/', (req, res) => {
      res.sendFile(path + "index.html");
  });

  //Api - to get stored stats.
  app.get('/stats', function(req, res){

    new Promise(function (resolve, reject) {

      var responseObj;

      var fromDate = new Date();
      fromDate.setDate(fromDate.getDate() - 7);
      var milliseconds = fromDate.getTime(); 

      //use prepared so that it could be changed to a user defined param later.
      let sql = "SELECT * FROM system_stats WHERE time_now >= ?";
      db.all(sql, milliseconds, function cb(err, rows) {
        if (err) {
          responseObj = {
            'status': true,
            'error': err
          };
          reject(responseObj);
        } else {
          responseObj = {
            'status': true,
            rows
          };
          resolve(responseObj);
        }
      });

    }).then(function(result) { 
      //Return Results
      res.json(result);
    }).catch(function(e) {
      //If there was an issue we still need to return a result but with an error and we want to show a nice error.
      res.json({
          'status': false,
          'error': 'There was a problem connecting to the database, if the problem persists please contact support.'
      });
    });
        
  });

  //start the server
  http.listen(Config.localPort, () => console.log('App listening on port '+ Config.localPort +'!'));
}